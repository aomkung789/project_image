from re import X
from PIL.Image import Image
import cv2
import numpy as np
import pytesseract
from pytesseract.pytesseract import image_to_string

#init variable
studentID = ''

#----------------------------------------------------------------
#แก้ที่อยู่ของ Pytesseract เอาเองนะ OWEN 
pytesseract.pytesseract.tesseract_cmd = r'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
#----------------------------------------------------------------

# function set image
def setImage(img):
    img = cv2.resize(img, (800,500), interpolation=cv2.INTER_AREA)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.adaptiveThreshold(
        img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 97, 55)
    cv2.imshow('Result', img)
    return img

# function check IDStudent
def check_idStudent(student_id):
    for x in student_id:
        if len(x) == 8:
            studentID = x
    print("ID Card is "+studentID)


img = cv2.imread('image/61160159.jpg')
setImage(img)
result = pytesseract.image_to_string(img)
check_idStudent(result.split())


# hImg ,wImg = img.shape
# boxes = pytesseract.image_to_boxes(img)
# print(boxes)
# for b in boxes.splitlines():
#     #print (b)
#     b = b.split(' ')
#     #print(b)
#     x,y,w,h = int(b[1]),int(b[2]),int(b[3]),int(b[4])
#     cv2.rectangle(img,(x,hImg-y),(x+w,hImg+h),(0,0,255),1)
#     #cv2.putText(img,b[0],(x,hImg-y+25),cv2.FONT_HERSHEY_COMPLEX,1,(50,50,255),2)

cv2.waitKey(0)

