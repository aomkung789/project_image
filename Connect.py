from bs4 import BeautifulSoup
from requests_html import HTMLSession
from urllib.parse import urljoin
import re
from main import studentID


# initialize an HTTP session
session = HTMLSession()

def get_all_forms(url):
    """Returns all form tags found on a web page's `url` """
    # GET request
    res = session.get(url)
    
    # for javascript driven website
    # res.html.render()
    soup = BeautifulSoup(res.html.html, "html.parser")
    return soup.find_all("form")

def get_form_details(form):
    """Returns the HTML details of a form,
    including action, method and list of form controls (inputs, etc)"""
    details = {}
    # get the form action (requested URL)
    action = form.attrs.get("action").lower()
    # get the form method (POST, GET, DELETE, etc)
    # if not specified, GET is the default in HTML
    method = form.attrs.get("method", "get").lower()
    # get all form inputs
    inputs = []
    for input_tag in form.find_all("input"):
        # get type of input form control
        input_type = input_tag.attrs.get("type", "text")
        # get name attribute
        input_name = input_tag.attrs.get("name")
        # get the default value of that input tag
        input_value =input_tag.attrs.get("value", "")
        # add everything to that list
        inputs.append({"type": input_type, "name": input_name, "value": input_value})
    # put everything to the resulting dictionary
    details["action"] = action
    details["method"] = method
    details["inputs"] = inputs
    return details

url = "https://reg.buu.ac.th/registrar/learn_time.asp?avs873319734=1"
# get all form tags
"""
forms = get_all_forms(url)

# iteratte over forms

for i, form in enumerate(forms, start=1):
    form_details = get_form_details(form)
    print("="*50, f"form #{i}", "="*50)
    print(form_details)   
"""

# get the first form
first_form = get_all_forms(url)[1]
# extract all form details
form_details = get_form_details(first_form)
#print(form_details)
# the data body we want to submit
data = {}
#print(form_details["method"])

for input_tag in form_details["inputs"]:
    #print(input_tag)
    if input_tag["type"] == "HIDDEN":
        # if it's hidden, use the default value
        data[input_tag["name"]] = input_tag["value"]
    elif  input_tag["name"] == 'f_studentcode':
        value = studentID
        data[input_tag["name"]] = value
    elif input_tag["type"] != "SUBMIT" :
        # all others except submit, prompt the user to set it
        data[input_tag["name"]] = input_tag["value"]
# join the url with the action (form request URL)
url = urljoin(url, form_details["action"])
#print(url)
data['f_studentstatus'] = '0' 
data['f_maxrows'] = '25'
if form_details["method"] == "post":
    res = session.post(url, data=data)
    #print(res)
elif form_details["method"] == "get":
    res = session.get(url, params=data)

soup = BeautifulSoup(res.content, "lxml")
#print(soup)
#soup = BeautifulSoup(res.content,('utf-8', 'ignore'))
# write the page content to a file
open("page2.html", "w").write(str(soup))

mydivs = soup.find_all("table", {"class": "normaldetail"})
mydivs_detail = mydivs[0].find_all("td")

#print(mydivs_detail[8])
#print(mydivs_detail[9])
data2 = []
rows = mydivs[0].find_all('tr')
for row in rows:
    cols = row.find_all('td')
    cols = [ele.text.strip() for ele in cols]
    data2.append([ele for ele in cols if ele]) # Get rid of empty values

detail = data2[1][2]
#print(detail)
#ชื่อคณะ
detail_f = (data2[1][3])
word_check_thai = re.compile(r'[ก-๙]')
word_check_eng = re.compile(r'[a-zA-Z]')
studentNamethai = ''
studentNameeng = ''

for x in detail:
    if(word_check_thai.match(x)):
        studentNamethai += x
    elif(x==' '):
        studentNamethai += ' '
        studentNameeng += ' '
    elif(word_check_eng.match(x)):
        studentNameeng += x
        
print(studentID)        
print(studentNamethai)
print(studentNameeng.strip())
print(detail_f) 

    
    